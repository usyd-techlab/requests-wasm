"""WASM authentication module for requests.

Example:
import requests
import requests_wasm

auth = requests_wasm.WASMAuth('wasm.example.com','example_realm','username','password')

r = requests.get('https://protected.example.com/secret.txt',auth=auth)

s = requests.Session()
s.auth = auth
r = s.get('https://protected.example.com/secret.txt')

"""
__version__='0.0.1'

import requests

urlparse = requests.utils.urlparse
urlunparse = requests.utils.urlunparse
parse_qs = urlparse.__globals__['parse_qs']

class WASMAuth(requests.auth.AuthBase):
    """WASM authentication handler for requests"""
    def __init__(self, hostname, realm, username, password, session=requests, scheme='https',path='/login.cgi',params=None,query=None,fragment=None):
        super(WASMAuth, self).__init__()
        self.authurl = urlunparse([scheme,hostname,path,params,query,fragment])
        self.realm = realm
        self.username = username
        self.password = password
        self.session = session

    def __call__(self, r):
        """Register WASM authentication callback"""
        r.register_hook('response',self.auth_callback)
        return r

    def auth_callback(self,r,**kwargs):
        """Detects if WASM authentication service is waiting for input then triggers and returns an authentication request."""
        if r.status_code==200 and r.url.startswith(self.authurl):
            return self.session.post(r.url,data=self.get_auth_data(r.url),**kwargs)

    def get_auth_data(self,url):
        """Returns authentication form submission data."""
        data = {'appRealm':self.realm,'credential_0':self.username,'credential_1':self.password,'Submit':'Sign in'}
        data.update(parse_qs(urlparse(url).query))
        return data

requests-wasm
=============

WASM authentication module for requests.

Installation
------------

    pip install https://bitbucket.org/ictidam/requests-wasm/get/master.zip

Examples
--------

    import requests
    import requests_wasm

    auth = requests_wasm.WASMAuth('wasm.example.com','example_realm','username','password')

    r = requests.get('https://protected.example.com/secret.txt',auth=auth)

    s = requests.Session()
    s.auth = auth
    r = s.get('https://protected.example.com/secret.txt')